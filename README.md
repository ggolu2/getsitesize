# ABOUT #
Get size of downloadable part of a website. Based on wget.
# USAGE #

* `chmod +x ./getsitesize`
* `./getsitesize <URL>`
